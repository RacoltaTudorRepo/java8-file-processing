package control;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.MonitoredData;

public class Utilities {
	public Stream<String> file(String name){
		try {
			Path path=Paths.get(getClass().getClassLoader().getResource(name).toURI());
			return Files.lines(path);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<MonitoredData> getMonitoredData(Stream<String> content){
		ArrayList<MonitoredData> list=new ArrayList<MonitoredData>();
		DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		content.forEach(line->{
			String[] splitted=line.split("\t\t");
			LocalDateTime start=LocalDateTime.parse(splitted[0],format);
			LocalDateTime end=LocalDateTime.parse(splitted[1],format);
			String activity=splitted[2].trim();
			list.add(new MonitoredData(start,end,activity));
		});
		return list;
	}
	
	public void countDays(ArrayList<MonitoredData> list) {
		ArrayList<LocalDate> daysStart=(ArrayList<LocalDate>) list.stream().map(MonitoredData::getStart).map(LocalDateTime::toLocalDate).distinct().collect(Collectors.toList());
		System.out.println("Numar de zile distincte: "+daysStart.size());
	}
	public HashMap<String,Integer> countActivity(ArrayList<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException{
		HashMap<String,Integer> map=new HashMap<String,Integer>();
		list.stream().collect(Collectors.groupingBy(MonitoredData::getActivity)).values().stream()
		.forEach(sameActivity-> map.put(sameActivity.get(0).getActivity(), sameActivity.size())); //sameActivity-list of monitored data
		PrintWriter writer = new PrintWriter("Task2.txt", "UTF-8");
		map.entrySet().stream().forEach(e->{
			writer.println(e.getKey()+" a fost efectuata intr-un numar de "+e.getValue().toString());
		});
		writer.close();
		return map;
	}
	public void countActivityPerDay(ArrayList<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException {
		//Map<String, List<MonitoredData>> map=list.stream().collect(Collectors.groupingBy(MonitoredData::getActivity)); //map cu activitate si lista cu acea activitate de fiecare data cand apare ea
		//ArrayList<String> activities=(ArrayList<String>) list.stream().map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
		HashMap<Integer,HashMap<String,Integer>> map=new HashMap<Integer,HashMap<String,Integer>>();
		List<LocalDate> days=list.stream().map(MonitoredData::getStart).map(LocalDateTime::toLocalDate).distinct().collect(Collectors.toList());
		days.stream().forEach(day->{
			HashMap<String,Integer> aux=new HashMap<String,Integer>();
			list.stream().forEach(activity->{
				if(activity.getStart().getDayOfMonth()==day.getDayOfMonth())
					if(aux.get(activity.getActivity())!=null)
						aux.put(activity.getActivity(), aux.get(activity.getActivity())+1);
					else 
						aux.put(activity.getActivity(),1);
			
			});
			map.put(day.getDayOfMonth(), aux);
		});
		PrintWriter writer = new PrintWriter("Task3.txt", "UTF-8");
		map.entrySet().forEach(e->{
			writer.println("Ziua "+e.getKey());
			e.getValue().entrySet().forEach(activityPerDay->{
				writer.println("\t"+activityPerDay.getKey()+" a fost efectuata de "+activityPerDay.getValue()+" ori");
			});
		});
		writer.close();
	}
	public void countDuration(ArrayList<MonitoredData> list) throws FileNotFoundException, UnsupportedEncodingException{
		HashMap<String,Duration> map=new HashMap<String,Duration>();
		list.stream().forEach(line->{
			Duration duration=Duration.between(line.getStart(), line.getEnd());
			if(map.get(line.getActivity())!=null)
					map.put(line.getActivity(),map.get(line.getActivity()).plus(duration));
			else map.put(line.getActivity(),duration);
		});
		PrintWriter writer = new PrintWriter("Task4.txt", "UTF-8");
		map.entrySet().stream().forEach(e->{if(e.getValue().toHours()>10)
			writer.println(e.getKey()+" a durat "+e.getValue().toString());
		});	
		writer.close();
	}
	public void percentFilter(ArrayList<MonitoredData> list,HashMap<String,Integer> map) throws FileNotFoundException, UnsupportedEncodingException {//map- (numa activitate,de cate ori a aparut)
		ArrayList<String> filtredActivities=new ArrayList<String>();
		HashMap<String,Integer> distinctActivitiesWithUnder5Minutes=new HashMap<String,Integer>();//(nume activitate,de cate ori s-a intamplat sub 5 minute)
		list.stream().forEach(activity->{
			Duration duration=Duration.between(activity.getStart(),activity.getEnd());
			if(duration.toMinutes()<5)
				if(distinctActivitiesWithUnder5Minutes.get(activity.getActivity())!=null)
					distinctActivitiesWithUnder5Minutes.put(activity.getActivity(), distinctActivitiesWithUnder5Minutes.get(activity.getActivity())+1);
				else
					distinctActivitiesWithUnder5Minutes.put(activity.getActivity(),1);
		});
		distinctActivitiesWithUnder5Minutes.entrySet().forEach(entry->{
			String activity=entry.getKey();
			Integer timesUnder5Mins=entry.getValue();
			Integer activityOccurance=map.get(activity);
			double times=timesUnder5Mins.floatValue();
			double occurances=activityOccurance.floatValue();
			double percent=(occurances*0.9);
			if(times>=percent)
				filtredActivities.add(entry.getKey());
		});
		PrintWriter writer = new PrintWriter("Task5.txt", "UTF-8");
		filtredActivities.stream().forEach(activity->writer.println(activity));
		writer.close();
	}
}
	