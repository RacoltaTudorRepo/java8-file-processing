package model;

import java.time.LocalDateTime;

public class MonitoredData {
	private LocalDateTime startTime ;
	private LocalDateTime endTime;
	private String activity;
	
	public MonitoredData(LocalDateTime startTime,LocalDateTime endTime, String activity) {
		this.startTime=startTime;
		this.endTime=endTime;
		this.activity=activity;
	}
	
	public LocalDateTime getStart() {
		return startTime;
	}
	public LocalDateTime getEnd() {
		return endTime;
	}
	public String getActivity() {
		return activity;
	}
}
