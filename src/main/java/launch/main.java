package launch;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Stream;

import control.Utilities;
import model.MonitoredData;

public class main {
	public static void main(String[] args) throws IOException {
		Utilities uti=new Utilities();
		Stream<String> stream=uti.file("Activities.txt");
		ArrayList<MonitoredData> list= uti.getMonitoredData(stream);
		//0
		for(MonitoredData date:list) {
			System.out.println(date.getActivity());
		}
		
		//1
		System.out.println("");
		uti.countDays(list);
		
		//2
		HashMap<String,Integer> map=uti.countActivity(list);
		
		//3
		uti.countActivityPerDay(list);
		
		//4
		System.out.println("");
		uti.countDuration(list);
		
		//5
		uti.percentFilter(list, map);
		
	}
}

